'''
Essentially all of our experiments require us to use the HDMI port to write out the video at 60fps which means the color channels get displayed at 1440Hz.
We want to build software that is going to give us a high degree of certainty that data will display at the correct time.
The order that the channels are displayed in is: blue 0-7, red 0-7, green 0-7
'''
# import the necessary packages
from threading import Thread
import sys
import cv2

# from imutils.video import FileVideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
 
# import the Queue class from Python 3
if sys.version_info >= (3, 0):
    from queue import Queue
 
# otherwise, import the Queue class for Python 2.7
else:
    from Queue import Queue


def generate_video(array,filename,fps=60,x_dim = 684, y_dim = 608):
    # assert array.shape[1] == y_dim 
    # assert array.shape[2] == x_dim
    # assert array.shape[3] == 3 or array.shape[3] == 1
    # assert array.dtype == np.uint8

    import imageio
    imageio.mimwrite(filename,array, fps=fps)
    return 

# listy = np.random.randint(low=0,high=255,size=(600,608, 684,3),dtype=np.uint8)
# generate_video(listy,"Images/testing/video.mp4")

# start the file video stream thread and allow the buffer to
# start to fill
print("[INFO] starting video file thread...")
fvs = FileVideoStream("Images/testing/video.mp4",queue_size=256).start()
time.sleep(1.0)
 
# start the FPS timer
fps = FPS().start()

class subframe():
    def __init__(self):
        self.fvs = FileVideoStream("Images/testing/video.mp4",queue_size=256).start()
        time.sleep(1.0)
        self.fps = FPS().start()
        self.past_frame_time = 0
        self.num_subframes_passed = 0

    def subframe_ready(self):
        if time.time() - self.past_frame_time < self.num_subframes_passed * 0.00069444444: #1/1440
            return 0 #0 means not ready
        elif self.num_subframes_passed != 24:
            self.num_subframes_passed += 1
            return 1 #subframe ready
        elif self.num_subframes_passed == 24:
            self.past_frame_time == time.time()
            self.num_subframes_passed = 0 
            
            return 2 #subframe and frame ready

    def advance_frame(self):
        cv2.imshow("Frame",self.fvs.read())
        self.past_frame_time = time.time()
        cv2.waitKey(1)
        self.fps.update()

    def stop(self):
        self.fps.stop()

        print("[INFO] elasped time: {:.2f}".format(self.fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(self.fps.fps()))   

        cv2.destroyAllWindows()
        self.fvs.stop()

sf = subframe()
counts = 600
#We want to check if 24 subframes have passed
# while sf.fvs.more() and counts != 0:
#     sfr = sf.subframe_ready()
#     if sfr == 0: pass
#     elif sfr == 1: pass
#     elif sfr == 2: 
#         sf.advance_frame()
#         counts -= 1
while sf.fvs.more() and counts != 0:
    sf.advance_frame()
    counts -= 1

sf.stop() 

# counts = 600
# while fvs.more() and counts != 0:
#     cv2.imshow("Frame",fvs.read())
#     cv2.waitKey(1)
#     fps.update()
#     counts -= 1

# fps.stop()

# print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
# print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))  
 
# cv2.destroyAllWindows()
# fvs.stop()
