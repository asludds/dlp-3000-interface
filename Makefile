all: lcr_app output shared clean run_python

lcr_app: lcr_cmd.c lcr_packetizer.c tcp_client.c neuralnet.c
	gcc -Wall $^ -o $@

output:
	gcc -c -fPIC neuralnet.c -o neuralnet.o
	gcc -c -fPIC lcr_cmd.c -o lcr_cmd.o
	gcc -c -fPIC lcr_packetizer.c -o lcr_packetizer.o
	gcc -c -fPIC tcp_client.c -o tcp_client.o

shared:
	gcc -shared neuralnet.o lcr_cmd.o lcr_packetizer.o tcp_client.o -o libneuralnet.so

clean:
	rm *.o

run_python:
	python3 main.py
