
----------------------------------------------------------------------------------------------------------------------------------------------- 
Copyright � 2012-2013 Texas Instruments Incorporated - http://www.ti.com/        
-----------------------------------------------------------------------------------------------------------------------------------------------

Product name and version number: DLP LightCrafter API Sample Code-Linux v2.0
Company name: Texas Instruments

This file contains important information about LightCrafter's command interface sample Code. Complete information about DLP� LightCrafter� is available at http://www.ti.com/tool/dlplightcrafter

Why should I use it?
An example Linux application is created using the sample code. The code shows implementation of  the DM365 command interface to interact with the DLP LightCrafter. The command definitions are described in "DLP� LightCrafter� DM365 Command Interface Guide" document.

How should I use it?
Execute following commands to run the demo,
make clean
make lcr_app
And, run lcr_app in the terminal (execute ./lcr_app from this directory)

The demo provides the ability to chose between different display modes and save solutions

Important Note: The sample source code provided is compatible with DM365 v4.0 or later.

Contact us at:
http://e2e.ti.com/
Post your questions in DLP & MEMS Section of TI E2E Forum.


