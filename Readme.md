# DLP3000 LightCrafter Python Driver

I have a project that I am working on that requires for me to integrate the DLP3000 lightcrafter with python code (deep learning libraries). Integration occurs using the ctypes library, which is included in base Python3

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This software has been tested on python3.5, but should work for any python 3 distribution.

```
Give examples
```

### Installing

Download from the gitlab 


## Authors

Alexander Sludds

## Licence
Some parts of this software are copyrighted under texas instruments, so who knows.
