import ctypes
import time

class dlp3000():
    def __init__(self):
        self.dll = ctypes.CDLL("./libneuralnet.so")

    #Wrapper for main function
    def dmd_main(self):
        self.dll.main()
    
    #Wrapper for Versions function
    def dmd_firmware_versions(self):
        self.dll.Versions()

    #Wrapper for Display_HDMI
    def dmd_display_hdmi(self):
        self.dll.DisplayHDMI()

    #Wrapper for to open up dmd
    def dmd_open(self):
        self.dll.Open()

    #Wrapper for initialization
    def dmd_initialization(self,bitdepth,numpatterns):
        self.dll.Initialization(ctypes.c_int(bitdepth),ctypes.c_int(numpatterns))

    #Wrapper for DefineBMP
    def dmd_define_bmp(self,image_index,file_address):
        try:
            file_address.decode()
        except AttributeError:
            pass
        a = ctypes.c_char_p(file_address.encode())
        integer = ctypes.c_int(image_index)
        self.dll.DefineBMP(integer,a)

    def dmd_start_pattern(self):
        self.dll.StartPattern()

    def dmd_advance_pattern_sequence(self):
        self.dll.AdvancePatternSequence()

    # def dmd_display_black(self):
    #     c = ctypes.c_uint(0)
    #     self.dll.displayColor(c)

    # def dmd_display_white(self):
    #     c = ctypes.c_uint(2**32-1)
    #     self.dll.displayColor(c)

    def dmd_display_black(self):
        self.dll.displayBlack()

    def dmd_display_white(self):
        self.dll.displayWhite()

    def dmd_camera_trigger(self):
        self.dll.setCameraTrigger()

    def dmd_get_camera_trigger(self):
        self.dll.getCameraTrigger()

    def dmd_stop_pattern(self):
        self.dll.StopPattern()

    def dmd_close(self):
        self.dll.Close()

