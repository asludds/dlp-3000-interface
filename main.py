import time
from firmware import dlp3000
from PIL import Image
import numpy as np
from progress.bar import Bar

def generate_images(array,name):
    image = Image.fromarray(array)
    image = image.convert('1')
    image.save("./Images/example/" + str(name) +".bmp")

x, y = 608, 684
for i in range(96):
    array = np.zeros((y,x),"uint8")
    bsize = 6
    assert bsize*i < x
    array[:,bsize*i:bsize*(i+1)] = 255*np.ones((array.shape[0],bsize))
    generate_images(array,str(i))

# number_of_images = 96
number_of_images = 8

dlp = dlp3000()
dlp.dmd_open()
# dlp.dmd_main()



# num = 1000
# upper = 2**32-1
# start =  time.time()
# for i in range(num):
#     dlp.dmd_display_black()
#     dlp.dmd_display_white()

# average_time = (time.time()-start)/(2*num)
# print("fps",1./average_time)

# dlp.dmd_initialization(1, number_of_images)
dlp.dmd_display_hdmi()
# dlp.dmd_camera_trigger()
# dlp.dmd_get_camera_trigger()

# bar = Bar("Uploading images to DMD memory",max=number_of_images)
# for i in range(number_of_images):
#     string = "./Images/example/" + str(i) + ".bmp"
#     # string = "./Images/PatSeqImages/pattern_8_0" + str(i) + ".bmp"
#     dlp.dmd_define_bmp(i,string)
#     bar.next()
# bar.finish()

# dlp.dmd_start_pattern()

# start = time.time()
# for i in range(number_of_images):
#     time.sleep(0.05)
#     dlp.dmd_advance_pattern_sequence()
# print("Average time", (time.time()-start)/number_of_images)

# dlp.dmd_stop_pattern()
dlp.dmd_close()



