#include <unistd.h>

#include "common.h"
#include "error.h"
#include "lcr_cmd.h"

static void Versions();

static void mSleep(unsigned long int mSeconds);

void DisplayHDMI();

void Open();

void Initialization(int bitdepth, int numpatterns);

void DefineBMP(int string_num, char strings[]);

void StartPattern();

void AdvancePatternSequence();

void StopPattern();

void Close();

int main(int argc, char *argv[])
{
  Open();
  Versions();
  uint8 i;
  LCR_PatternSeqSetting_t patSeqSet;


  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x04));

  patSeqSet.BitDepth = 1;
  patSeqSet.NumPatterns = 8;
  patSeqSet.PatternType = PTN_TYPE_NORMAL;
  patSeqSet.InputTriggerDelay = 0;
  /* patSeqSet.InputTriggerType = TRIGGER_TYPE_AUTO; */
  patSeqSet.InputTriggerType = TRIGGER_TYPE_SW;
  /* patSeqSet.AutoTriggerPeriod = 8700; */
  /* patSeqSet.ExposureTime = 8333; */


  /* patSeqSet.AutoTriggerPeriod = 600; */
  /* patSeqSet.ExposureTime = 600; */
  patSeqSet.LEDSelect = LED_GREEN;
  patSeqSet.Repeat = 0;
  LCR_CMD_SetPatternSeqSetting(&patSeqSet);

  if(patSeqSet.BitDepth == 1 && patSeqSet.NumPatterns == 8){
    printf("Uploading 8 1 bit images \n");
    LCR_CMD_DefinePatternBMP(0,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(1,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(2,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(3,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(4,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(5,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(6,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(7,"./Images/PatSeqImages/pattern_1_07.bmp");
    printf("Upload finished");
  } else if(patSeqSet.BitDepth == 1 && patSeqSet.NumPatterns == 32){
    printf("Uploading 32 1 bit images \n");
    LCR_CMD_DefinePatternBMP(0,"./Images/example/0.bmp");
    LCR_CMD_DefinePatternBMP(1,"./Images/example/1.bmp");
    LCR_CMD_DefinePatternBMP(2,"./Images/example/2.bmp");
    LCR_CMD_DefinePatternBMP(3,"./Images/example/3.bmp");
    LCR_CMD_DefinePatternBMP(4,"./Images/example/4.bmp");
    LCR_CMD_DefinePatternBMP(5,"./Images/example/5.bmp");
    LCR_CMD_DefinePatternBMP(6,"./Images/example/6.bmp");
    LCR_CMD_DefinePatternBMP(7,"./Images/example/7.bmp");
    LCR_CMD_DefinePatternBMP(8,"./Images/example/8.bmp");
    LCR_CMD_DefinePatternBMP(9,"./Images/example/9.bmp");
    LCR_CMD_DefinePatternBMP(10,"./Images/example/10.bmp");
    LCR_CMD_DefinePatternBMP(11,"./Images/example/11.bmp");
    LCR_CMD_DefinePatternBMP(12,"./Images/example/12.bmp");
    LCR_CMD_DefinePatternBMP(13,"./Images/example/13.bmp");
    LCR_CMD_DefinePatternBMP(14,"./Images/example/14.bmp");
    LCR_CMD_DefinePatternBMP(15,"./Images/example/15.bmp");
    LCR_CMD_DefinePatternBMP(16,"./Images/example/16.bmp");
    LCR_CMD_DefinePatternBMP(17,"./Images/example/17.bmp");
    LCR_CMD_DefinePatternBMP(18,"./Images/example/18.bmp");
    LCR_CMD_DefinePatternBMP(19,"./Images/example/19.bmp");
    LCR_CMD_DefinePatternBMP(20,"./Images/example/20.bmp");
    LCR_CMD_DefinePatternBMP(21,"./Images/example/21.bmp");
    LCR_CMD_DefinePatternBMP(22,"./Images/example/22.bmp");
    LCR_CMD_DefinePatternBMP(23,"./Images/example/23.bmp");
    LCR_CMD_DefinePatternBMP(24,"./Images/example/24.bmp");
    LCR_CMD_DefinePatternBMP(25,"./Images/example/25.bmp");
    LCR_CMD_DefinePatternBMP(26,"./Images/example/26.bmp");
    LCR_CMD_DefinePatternBMP(27,"./Images/example/27.bmp");
    LCR_CMD_DefinePatternBMP(28,"./Images/example/28.bmp");
    LCR_CMD_DefinePatternBMP(29,"./Images/example/29.bmp");
    LCR_CMD_DefinePatternBMP(30,"./Images/example/30.bmp");
    LCR_CMD_DefinePatternBMP(31,"./Images/example/31.bmp");
    printf("Upload finished \n");
  } else if(patSeqSet.BitDepth == 1 && patSeqSet.NumPatterns == 96){
    LCR_CMD_DefinePatternBMP(0,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(1,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(2,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(3,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(4,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(5,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(6,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(7,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(8,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(9,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(10,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(11,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(12,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(13,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(14,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(15,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(16,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(17,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(18,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(19,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(20,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(21,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(22,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(23,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(24,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(25,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(26,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(27,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(28,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(29,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(30,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(31,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(32,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(33,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(34,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(35,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(36,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(37,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(38,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(39,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(40,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(41,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(42,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(43,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(44,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(45,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(46,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(47,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(48,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(49,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(50,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(51,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(52,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(53,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(54,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(55,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(56,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(57,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(58,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(59,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(60,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(61,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(62,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(63,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(64,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(65,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(66,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(67,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(68,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(69,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(70,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(71,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(72,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(73,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(74,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(75,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(76,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(77,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(78,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(79,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(80,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(81,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(82,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(83,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(84,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(85,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(86,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(87,"./Images/PatSeqImages/pattern_1_07.bmp");
    LCR_CMD_DefinePatternBMP(88,"./Images/PatSeqImages/pattern_1_00.bmp");
    LCR_CMD_DefinePatternBMP(89,"./Images/PatSeqImages/pattern_1_01.bmp");
    LCR_CMD_DefinePatternBMP(90,"./Images/PatSeqImages/pattern_1_02.bmp");
    LCR_CMD_DefinePatternBMP(91,"./Images/PatSeqImages/pattern_1_03.bmp");
    LCR_CMD_DefinePatternBMP(92,"./Images/PatSeqImages/pattern_1_04.bmp");
    LCR_CMD_DefinePatternBMP(93,"./Images/PatSeqImages/pattern_1_05.bmp");
    LCR_CMD_DefinePatternBMP(94,"./Images/PatSeqImages/pattern_1_06.bmp");
    LCR_CMD_DefinePatternBMP(95,"./Images/PatSeqImages/pattern_1_07.bmp");
    printf("Upload complete");
  }

  else if(patSeqSet.BitDepth == 8 && patSeqSet.NumPatterns == 8){
    printf("Uploading 8 bit images stock \n");
    LCR_CMD_DefinePatternBMP(0,"./Images/PatSeqImages/pattern_8_00.bmp");
    LCR_CMD_DefinePatternBMP(1,"./Images/PatSeqImages/pattern_8_01.bmp");
    LCR_CMD_DefinePatternBMP(2,"./Images/PatSeqImages/pattern_8_02.bmp");
    LCR_CMD_DefinePatternBMP(3,"./Images/PatSeqImages/pattern_8_03.bmp");
    LCR_CMD_DefinePatternBMP(4,"./Images/PatSeqImages/pattern_8_04.bmp");
    LCR_CMD_DefinePatternBMP(5,"./Images/PatSeqImages/pattern_8_05.bmp");
    LCR_CMD_DefinePatternBMP(6,"./Images/PatSeqImages/pattern_8_06.bmp");
    LCR_CMD_DefinePatternBMP(7,"./Images/PatSeqImages/pattern_8_07.bmp");
    printf("Upload finised \n");
  } else if (patSeqSet.BitDepth == 8 && patSeqSet.NumPatterns == 32) {
    printf("Uploading 8 bit images custom \n");
    LCR_CMD_DefinePatternBMP(0,"./Images/PatSeqImages/pattern_8_00.bmp");
    LCR_CMD_DefinePatternBMP(1,"./Images/PatSeqImages/pattern_8_01.bmp");
    LCR_CMD_DefinePatternBMP(2,"./Images/PatSeqImages/pattern_8_02.bmp");
    LCR_CMD_DefinePatternBMP(3,"./Images/PatSeqImages/pattern_8_03.bmp");
    LCR_CMD_DefinePatternBMP(4,"./Images/PatSeqImages/pattern_8_04.bmp");
    LCR_CMD_DefinePatternBMP(5,"./Images/PatSeqImages/pattern_8_05.bmp");
    LCR_CMD_DefinePatternBMP(6,"./Images/PatSeqImages/pattern_8_06.bmp");
    LCR_CMD_DefinePatternBMP(7,"./Images/PatSeqImages/pattern_8_07.bmp");
    LCR_CMD_DefinePatternBMP(8,"./Images/PatSeqImages/pattern_8_00.bmp");
    LCR_CMD_DefinePatternBMP(9,"./Images/PatSeqImages/pattern_8_01.bmp");
    LCR_CMD_DefinePatternBMP(10,"./Images/PatSeqImages/pattern_8_02.bmp");
    LCR_CMD_DefinePatternBMP(11,"./Images/PatSeqImages/pattern_8_03.bmp");
    LCR_CMD_DefinePatternBMP(12,"./Images/PatSeqImages/pattern_8_04.bmp");
    LCR_CMD_DefinePatternBMP(13,"./Images/PatSeqImages/pattern_8_05.bmp");
    LCR_CMD_DefinePatternBMP(14,"./Images/PatSeqImages/pattern_8_06.bmp");
    LCR_CMD_DefinePatternBMP(15,"./Images/PatSeqImages/pattern_8_07.bmp");
    LCR_CMD_DefinePatternBMP(16,"./Images/PatSeqImages/pattern_8_00.bmp");
    LCR_CMD_DefinePatternBMP(17,"./Images/PatSeqImages/pattern_8_01.bmp");
    LCR_CMD_DefinePatternBMP(18,"./Images/PatSeqImages/pattern_8_02.bmp");
    LCR_CMD_DefinePatternBMP(19,"./Images/PatSeqImages/pattern_8_03.bmp");
    LCR_CMD_DefinePatternBMP(20,"./Images/PatSeqImages/pattern_8_04.bmp");
    LCR_CMD_DefinePatternBMP(21,"./Images/PatSeqImages/pattern_8_05.bmp");
    LCR_CMD_DefinePatternBMP(22,"./Images/PatSeqImages/pattern_8_06.bmp");
    LCR_CMD_DefinePatternBMP(23,"./Images/PatSeqImages/pattern_8_07.bmp");
    LCR_CMD_DefinePatternBMP(24,"./Images/PatSeqImages/pattern_8_00.bmp");
    LCR_CMD_DefinePatternBMP(25,"./Images/PatSeqImages/pattern_8_01.bmp");
    LCR_CMD_DefinePatternBMP(26,"./Images/PatSeqImages/pattern_8_02.bmp");
    LCR_CMD_DefinePatternBMP(27,"./Images/PatSeqImages/pattern_8_03.bmp");
    LCR_CMD_DefinePatternBMP(28,"./Images/PatSeqImages/pattern_8_04.bmp");
    LCR_CMD_DefinePatternBMP(29,"./Images/PatSeqImages/pattern_8_05.bmp");
    LCR_CMD_DefinePatternBMP(30,"./Images/PatSeqImages/pattern_8_06.bmp");
    LCR_CMD_DefinePatternBMP(31,"./Images/PatSeqImages/pattern_8_07.bmp");
  } else {
    return -1;
  }

  printf("Starting Pattern Sequence...\n");
  LCR_CMD_StartPatternSeq(1);
  i = patSeqSet.NumPatterns;
  while(i--){
    LCR_CMD_AdvancePatternSeq();
    mSleep(1000);
  }
  LCR_CMD_StartPatternSeq(0);
  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x00)); //Setting back to static display mode

  return 0;
}

void Open()
{
  if(LCR_CMD_Open())
  {
    printf("Unable to connect to the DLP LCr\n");
  }
}

void Initialization(int bitdepth, int numpatterns)
{
  Open();
  LCR_PatternSeqSetting_t patSeqSet;
  uint i;
  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x04));

  patSeqSet.BitDepth = bitdepth;
  patSeqSet.NumPatterns = numpatterns;

  patSeqSet.PatternType = PTN_TYPE_NORMAL;
  patSeqSet.InputTriggerDelay = 0;
  patSeqSet.InputTriggerType = TRIGGER_TYPE_SW;

  /* patSeqSet.AutoTriggerPeriod = 16667; */
  /* patSeqSet.ExposureTime = 16667; */

  patSeqSet.LEDSelect = LED_GREEN;
  patSeqSet.Repeat = 0;
  LCR_CMD_SetPatternSeqSetting(&patSeqSet);

  printf("End of initialization \n");
}

void DefineBMP(int string_num, char strings[])
{
  LCR_CMD_DefinePatternBMP(string_num,strings);
}

void StartPattern()
{
  printf("Starting Pattern Sequence...\n");
  LCR_CMD_StartPatternSeq(1);

}

void AdvancePatternSequence()
{
  LCR_CMD_AdvancePatternSeq();
}

void StopPattern()
{
  LCR_CMD_StartPatternSeq(0);
  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x00)); //Setting back to static display mode
  printf("Ending pattern sequence \n");
}

void Close()
{
  LCR_CMD_Close();
}

void DisplayHDMI()
{
  printf("Displaying HDMI");
  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x02));
}


static void mSleep(unsigned long int mSeconds)
{
  sleep((mSeconds/1000.0));
}

static void Versions()
{
  char verName[LCR_CMD_VERSION_STR_LEN];

  LCR_CMD_GetRevision(REV_DM365,&verName[0]);
  printf("DM365 Revision: %s\n",verName);

  LCR_CMD_GetRevision(REV_FPGA,&verName[0]);
  printf("FPGA Revision: %s\n",verName);

  LCR_CMD_GetRevision(REV_MSP430,&verName[0]);
  printf("MSP430 Revision: %s\n",verName);
}

void displayColor(uint32 Color)
{
  LCR_CMD_DisplayStaticColor(Color);
}

void displayBlack()
{
  LCR_PatternSeqSetting_t patSeqSet;

  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x04));

  patSeqSet.BitDepth = 1;
  patSeqSet.NumPatterns = 1;
	patSeqSet.PatternType = PTN_TYPE_HW;
  patSeqSet.InputTriggerType = TRIGGER_TYPE_AUTO;
	patSeqSet.InputTriggerDelay = 0;
  // patSeqSet.AutoTriggerPeriod = 16667;
  // patSeqSet.ExposureTime = 16667;
    // patSeqSet.LEDSelect = LED_BLUE;
	// patSeqSet.Repeat = 0;
  LCR_CMD_SetPatternSeqSetting(&patSeqSet);

	//Define Hardware Patterns
	LCR_HWPatternSeqDef_t hwPatDef;
	hwPatDef.index = 0;
	hwPatDef.numOfPatn = 1;
  hwPatDef.hwPatArray[0].Number = 0x1;
  hwPatDef.hwPatArray[0].Invert = 0;

	LCR_CMD_DefineHWPatSequence(&hwPatDef);
}

void displayWhite()
{
  LCR_PatternSeqSetting_t patSeqSet;

  LCR_CMD_SetDisplayMode((LCR_DisplayMode_t)(0x04));

  patSeqSet.BitDepth = 1;
  patSeqSet.NumPatterns = 1;
	patSeqSet.PatternType = PTN_TYPE_HW;
  patSeqSet.InputTriggerType = TRIGGER_TYPE_AUTO;
	patSeqSet.InputTriggerDelay = 0;
  LCR_CMD_SetPatternSeqSetting(&patSeqSet);

	//Define Hardware Patterns
	LCR_HWPatternSeqDef_t hwPatDef;
	hwPatDef.index = 0;
	hwPatDef.numOfPatn = 1;
  hwPatDef.hwPatArray[0].Number = 0x2;
  hwPatDef.hwPatArray[0].Invert = 0;

	LCR_CMD_DefineHWPatSequence(&hwPatDef);
}

void setCameraTrigger()
{
      // Set trigger output settings
    LCR_CamTriggerSetting_t triggerSettings;
    triggerSettings.Enable = 1;
    triggerSettings.Source = 0;
    triggerSettings.Polarity = TRIGGER_EDGE_POS;
    triggerSettings.Delay = 0;
    triggerSettings.PulseWidth = 200; //us
    LCR_CMD_SetCamTriggerSetting(&triggerSettings);
}

void getCameraTrigger()
{
  //Get trigger setttings
  LCR_CamTriggerSetting_t triggerSettings;
  LCR_CMD_GetCamTriggerSetting(&triggerSettings);
  printf("Enable %u \n",triggerSettings.Enable);
  printf("Source %u \n",triggerSettings.Source);
  printf("Delay %u \n",triggerSettings.Delay);
  printf("Polarity %u \n",triggerSettings.Polarity);
  printf("PulseWidth %u \n",triggerSettings.PulseWidth);
}